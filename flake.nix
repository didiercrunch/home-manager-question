{
  description = "Question about home-manager.";

  inputs = {
    home-manager.url = "github:nix-community/home-manager";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { home-manager, nixpkgs, self,... }:
    let
      ubuntuSystem = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.x86_64-linux ;
        modules = [
          {
            nixpkgs = {
              config = { allowUnfree = true; };
            };
            imports = [./ubuntu.nix];
          }
          {
            home = {
              username = "didier";
              homeDirectory = "/home/didier";
              stateVersion = "22.11";
            };
          }
        ];
      };
      in 
        {
          defaultPackage.x86_64-linux = ubuntuSystem.activationPackage;
        };
}
