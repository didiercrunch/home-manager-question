# Home manager options: https://nix-community.github.io/home-manager/options.html

{config
, pkgs
,...}: 
{
  home.packages = with pkgs; 
  [clojure 
  jq
  # import ./custom/zig.nix  << this does not work.
  ];
}
